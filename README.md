# AGOSUV-bench

This benchmark is composed of ontologies 
1. Adolena (A)
1. StockExchange(S)
1. University (U)
1. Vicodi (V)
1. Galen (G)
1. OBOprotein (O)

Note 
 - ASUV was introduced in (Pérez-Urbina, Horrocks and Motik, ISWC 2009) and is available at www.cs.ox.ac.uk/projects/requiem/evaluation.html.
- the DL-Lite versions of OpenGALEN2 (www.opengalen.org) and OBOprotein (www.obofoundry.org/), which come from a test suite proposed with the tool Rapid (Trivela, Stoilos, Chortaras and Stamou, DL 2013), available at www.image.ece.ntua.gr/~achort/rapid/.
- Each ontology is provided with 5 handcrafted queries.

